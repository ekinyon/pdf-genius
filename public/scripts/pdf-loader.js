// fileInput is an HTMLInputElement: <input type="file" id="myfileinput" multiple>
var fileInput = document.getElementById("files");

// This global variable is used to identify which card is being moved from the evidence buckets,
// in the right-most div, to the middle div for editing.
var moving_card_index = 0;
var card_number = 0;
var cards = {};

var startX = 0;
var startY = 0;
var endX = 0;
var endY = 0;

var TOOL = "SELECT";
var MOUSE_DOWN = false;

var _PDF_DOC,
    _CURRENT_PAGE,
    _TOTAL_PAGES,
    _PAGE_RENDERING_IN_PROGRESS = 0,
    _CANVAS = document.querySelector('#pdf-canvas');

// initialize and load the PDF
async function showPDF() {
    document.querySelector("#pdf-loader").style.display = 'block';

    let files = fileInput.files;
    let pdf_url = URL.createObjectURL(files[0]);
    // get handle of pdf document
    try {
        _PDF_DOC = await pdfjsLib.getDocument({ url: pdf_url });
    }
    catch(error) {
        alert(error.message);
    }

    // total pages in pdf
    _TOTAL_PAGES = _PDF_DOC.numPages;
    
    // Hide the pdf loader and show pdf container
    document.querySelector("#pdf-loader").style.display = 'none';
    document.querySelector("#pdf-contents").style.display = 'block';
    document.querySelector("#pdf-total-pages").innerHTML = _TOTAL_PAGES;

    // show the first page
    showPage(1);
}

/**
 * load and render specific page of the PDF
 * @param page_no
 * @returns {Promise<void>}
 */
async function showPage(page_no) {
    _PAGE_RENDERING_IN_PROGRESS = 1;
    _CURRENT_PAGE = page_no;

    // disable Previous & Next buttons while page is being loaded
    document.querySelector("#pdf-next").disabled = true;
    document.querySelector("#pdf-prev").disabled = true;

    // while page is being rendered hide the canvas and show a loading message
    document.querySelector("#pdf-canvas").style.display = 'none';
    document.querySelector("#page-loader").style.display = 'block';

    // update current page
    document.querySelector("#pdf-current-page").innerHTML = page_no;
    
    // get handle of page
    try {
        var page = await _PDF_DOC.getPage(page_no);
    }
    catch(error) {
        alert(error.message);
    }

    // original width of the pdf page at scale 1
    var pdf_original_width = page.getViewport(1).width;
    
    // as the canvas is of a fixed width we need to adjust the scale of the viewport where page is rendered
    var scale_required = _CANVAS.width / pdf_original_width;

    // get viewport to render the page at required scale
    var viewport = page.getViewport(scale_required);

    // set canvas height same as viewport height
    _CANVAS.height = viewport.height;

    // setting page loader height for smooth experience
    document.querySelector("#page-loader").style.height =  _CANVAS.height + 'px';
    document.querySelector("#page-loader").style.lineHeight = _CANVAS.height + 'px';

    // page is rendered on <canvas> element
    var render_context = {
        canvasContext: _CANVAS.getContext('2d'),
        viewport: viewport
    };
        
    // render the page contents in the canvas
    try {
        await page.render(render_context);
    }
    catch(error) {
        alert(error.message);
    }

    _PAGE_RENDERING_IN_PROGRESS = 0;

    // re-enable Previous & Next buttons
    document.querySelector("#pdf-next").disabled = false;
    document.querySelector("#pdf-prev").disabled = false;

    // show the canvas and hide the page loader
    document.querySelector("#pdf-canvas").style.display = 'block';
    document.querySelector("#page-loader").style.display = 'none';
}

/**
 * click on "Show PDF" buuton
 */
document.querySelector("#show-pdf-button").addEventListener('click', function() {
    showPDF();
});

/**
 * click on the "Previous" page button
 */
document.querySelector("#pdf-prev").addEventListener('click', function() {
    if(_CURRENT_PAGE != 1)
        showPage(--_CURRENT_PAGE);
});

/**
 * click on the "Next" page button
 */
document.querySelector("#pdf-next").addEventListener('click', function() {
    if(_CURRENT_PAGE != _TOTAL_PAGES)
        showPage(++_CURRENT_PAGE);
});

/**
 *
 */
document.getElementById("canvas-div").addEventListener("mousedown", function(event){
    if (TOOL === "Capture") {
        MOUSE_DOWN = true;
        startX = event.clientX;
        startY = event.clientY;


        showCrossHairOverlay(event);
    }
});

/**
 *
 */
document.addEventListener("mousemove", function(event) {
    moveCrossHairOverlay(event);
});

/**
 *
 */
document.addEventListener("mouseup", function(event){
    if (TOOL === "Capture" && MOUSE_DOWN) {
        MOUSE_DOWN = false;
        endX = event.clientX;
        endY = event.clientY;
        hideCrossHairOverlay(event);
        capture();
    }
});

/**
 * This function is responsible for enabling/disabling the selection tool
 * @param button The button that is being pressed.
 */
function handleToolChange(button) {
    TOOL = button.id;
}

/**
 * The function that is responsible for orchestrating the capture.
 */
function capture() {
   html2canvas(document.querySelector('body')).then(canvas => {

       // If the user drags from right to left, adjust for the reversed orientation
       if (startX > endX) {
           let temp = startX;
           startX = endX;
           endX = temp;
       }

       // If the user drags from bottom to top, adjust for the reversed orientation
       if (startY > endY) {
           let temp = startY;
           startY = endY;
           endY = temp;
       }

       // Get the data from the canvas by taking a
       let croppedCanvas = document.createElement('canvas');
       let width = (endX - startX) * 2;
       let height = (endY - startY) * 2;
       croppedCanvas.width = width;
       croppedCanvas.height = height;
       croppedCanvas.id = 'croppedCanvas';

       var croppedCanvasContext = croppedCanvas.getContext('2d');
       croppedCanvasContext.drawImage(canvas, startX*2, startY*2, width, height, 0, 0, width, height);

       // Reset the global variables back to zero for the next screen-shot.
       startX = 0;
       startY= 0;
       endX= 0;
       endY = 0;

       // Increment the card number, the primary key, by 1
       card_number += 1;

       let card_data = {'page': _CURRENT_PAGE, 'category': '', 'value': 0, 'text': '', 'img': croppedCanvas.toDataURL()};
       initCardWithData(card_number, card_data);
    });
}

/**
 * Once the evidence is in the middle div, and the details have been set,
 * the "Save" button will invoke this function to put the evidence in the
 * respective div to the right.
 * @param button The save button that is being pressed.
 */
function save_evidence(button) {
    let id = button.id;
    let index = id.split("_")[3];
    updateEvidence(index);
    document.getElementById('rubric-holder').innerHTML = "";
}

/**
 * A simple function used in the visuals of the grabbing of the screen-shot.
 */
function off() {
    document.getElementById("overlay").style.display = "none";
}

/**
 * A simple function used in the grabbing of the screen-shot.
 * @param event
 */
function showCrossHairOverlay(event){
    if (TOOL === "Capture") {
        var cross_hair_overlay = document.getElementById("cross-hair-overlay");
        cross_hair_overlay.style.display = "block";

        var cross_hair = document.getElementById("cross-hair");
        cross_hair.style.display = "block";
        cross_hair.style.top = event.clientY + "px";
        cross_hair.style.left = event.clientX + "px";
    }
}

/**
 * A simple function used in the grabbing of the screen-shot. This function
 * monitors the mouses location and updates the greyed overlay that indicates
 * which part of the PDF is being selected.
 * @param event The mouse event that is occurring.
 */
function moveCrossHairOverlay(event) {
    if (TOOL == "Capture" && MOUSE_DOWN) {
        var cross_hair = document.getElementById("cross-hair");
        cross_hair.style.display = "block";
        var left = parseInt(cross_hair.style.left.split("px")[0]);
        var top = parseInt(cross_hair.style.top.split("px")[0]);

        cross_hair.style.width = (event.clientX - left) + "px";
        cross_hair.style.height = (event.clientY - top) + "px";

    }
}

/**
 * A simple function used in the grabbing of the screen-shot. This function hides
 * the greyed overlay to indicate that the screen-shot cycle has ended.
 * @param event The mouse event that is occurring.
 */
function hideCrossHairOverlay(event){
    if (TOOL === "Capture") {
        document.getElementById("cross-hair-overlay").style.display = "none";
        document.getElementById("cross-hair").style.display = "none";
    }
}


/**
 *
 * @param card_number The unique number of the card that will act as a
 * primary key in the cards dictionary, as well as in the id creation of
 * its sub-elements.
 * @param card_data The data about the card that will be used to populate
 * the UI fields as well as the cards dictionary.
 */
function initCardWithData(card_number, card_data) {
    let flip_card = document.createElement('div');
    flip_card.className = 'flip-card';
    flip_card.id = 'flip-card-' + card_number;

    let flip_card_inner = document.createElement('div');
    flip_card_inner.className = 'flip-card-inner';

    let flip_card_font = document.createElement('div');
    flip_card_font.className = 'flip-card-front';

    let image = document.createElement('img');
    image.src = card_data.img;
    image.id = 'flip-card-image-' + card_number;
    image.className = 'flip-card-image';

    flip_card_font.appendChild(image);

    let break_div = document.createElement('div');
    break_div.innerHTML = '<br>';

    let flip_card_back = document.createElement('div');
    flip_card_back.className = 'flip-card-back';
    flip_card_back.innerHTML = '<h4><b>Pg: ' + card_data.page + '</b></h4>';
    flip_card_back.innerHTML += '<br>' +
        '<input onchange="radioValueChange(this)" type="radio" id="card_value_demographic_' + card_number + '"  name="card_radio_value_' + card_number + '" value="Demographic">' +
        '<label id="label_card_value_demographic_' + card_number + '" for="card_value_demographic_' + card_number + '">Demographic</label><br>' +
        '<input onchange="radioValueChange(this)" type="radio" id="card_value_academic_' + card_number + '" name="card_radio_value_' + card_number + '" value="Academic">' +
        '<label id="label_card_value_academic_' + card_number + '" for="card_value_academic_' + card_number + '">Academic</label><br>' +
        '<input onchange="radioValueChange(this)" type="radio" id="card_value_non_academic_' + card_number + '" name="card_radio_value_' + card_number + '" value="Non-Academic">' +
        '<label id="label_card_value_non_academic_' + card_number + '" for="card_value_non_academic_' + card_number + '">Non-Academic</label><br>' +
        '<input onchange="radioValueChange(this)" type="radio" id="card_value_alignment_' + card_number + '" name="card_radio_value_' + card_number + '" value="Alignment">' +
        '<label id="label_card_value_alignment_' + card_number + '" for="card_value_alignment_' + card_number + '">Alignment</label><br>' +
        '-3 <input onchange="sliderValueChange(this)" type="range" id="card_slider_' + card_number + '" name="card_slider' + card_number + '" min="-3" max="3"> 3' +
        '<textarea style="width: 90%;" id="card_notes_' + card_number + '" rows="4" cols="25" onchange="textAreaChange(this)">' + card_data.text + '</textarea>' +
        '<br>' +
        '<br>' +
        '<button id="card_save_button_' + card_number + '" type="button" onclick="save_evidence(this)">Save Evidence</button>' +
        '<br>' +
        '<button id="card_erase_button_' + card_number + '" type="button" onclick="erase_evidence(this)">Erase Evidence</button>'
    ;

    flip_card_inner.appendChild(flip_card_font);
    flip_card_inner.appendChild(flip_card_back);

    flip_card.appendChild(flip_card_inner);

    document.getElementById('rubric-holder').appendChild(flip_card);
    cards[card_number] = {'page': card_data.page, 'category': card_data.category, 'value': card_data.value, text: card_data.text, 'img': image.src};
}


/**
 * Function to adjust the card_data stored in the cards dictionary.
 * It takes the current value from the slider and uses it to update
 * the data.
 * @param slider The slider object.
 */
function sliderValueChange(slider) {
    // Obtain the index of the card_data in cards, from the id of the slider object
    let id = slider.id;
    let index = id.split("_")[2];

    // Figure out which radio button is selected, so that the slider value is
    // accurately mapped to the right evidence category.
    let radios = document.getElementsByName("card_radio_value_" + index);
    var checked = "";
    for(var i = 0; i < radios.length; i++) {
        if(radios[i].checked) {
            checked = radios[i].value;
        }
    }

    // Formatting
    checked = checked.replace("-", "_");
    checked = checked.toLowerCase();

    // Update the category of the card_data as well as the value associated with it.
    cards[index]['category'] = checked;
    cards[index]['value'] = document.getElementById("card_slider_" + index).value;
}

/**
 * When the text area within the UI div of the evidence card changes,
 * update the value that is stored in cards[index]['text']
 * @param text_area The text object that has a changed value.
 */
function textAreaChange(text_area){
    let text = text_area.value;
    let id = text_area.id;
    let index = id.split('_')[2];
    cards[index]['text'] = text;
}

/**
 *
 * @param radio_button
 */
function radioValueChange(radio_button) {
    let subs = radio_button.id.split("_");
    let length = subs.length;
    if (length === 5) {
        let category = subs[2] + "_" + subs[3];
        let index = subs[4];
        let card = cards[index];
        if (card) {
            card.category = category;
        }
    } else {
        let category = subs[2];
        let index = subs[3];
        let card = cards[index];
        if (card) {
            card.category = category;
        }
    }
}

function editCard(index) {
    // Remove the snapshot from evidence
    document.getElementById("evidence_div_" + index).remove();

    // Get the data card
    let dataCard = cards[index];
    delete cards[index];
    initCardWithData(index, dataCard);
    let radioID = "card_value_" + cards[index].category.toLowerCase() + "_" + index;
    radioID.replace("-", "_");
    document.getElementById(radioID).checked = true;
    document.getElementById( "card_slider_" + index).value = dataCard.value;
}

function updateEvidence(index) {
    let radios = document.getElementsByName("card_radio_value_" + index);
    var checked = "";
    for(var i = 0; i < radios.length; i++) {
        if(radios[i].checked) {
            checked = radios[i].value;
        }
    }


    let evidenceSnapshotDiv = document.createElement('div');
    evidenceSnapshotDiv.className = "evidence-div";
    evidenceSnapshotDiv.id = "evidence_div_" + index;
    evidenceSnapshotDiv.draggable = true;

    let evidenceSnapshotColor = document.createElement('span');
    evidenceSnapshotColor.height = "25px";
    evidenceSnapshotColor.width = "25px";

    if (document.getElementById('card_slider_' + index).value > 0) {
        evidenceSnapshotColor.className = "green-dot";
    } else if (document.getElementById('card_slider_' + index).value < 0) {
        evidenceSnapshotColor.className = "red-dot";
    } else {
        evidenceSnapshotColor.className = "black-dot";
    }

    let evidenceSnapshotImage = document.createElement('img');
    evidenceSnapshotImage.src = document.getElementById('flip-card-image-' + card_number).src;
    evidenceSnapshotImage.className = "evidence-image";

    evidenceSnapshotDiv.append(evidenceSnapshotColor);
    evidenceSnapshotDiv.append(evidenceSnapshotImage);
    evidenceSnapshotDiv.innerHTML += "<br>";

    if (checked === "Demographic") {
        document.getElementById('demographic-rubric-evidence').append(evidenceSnapshotDiv);
    } else if (checked === "Academic") {
        document.getElementById('academic-rubric-evidence').append(evidenceSnapshotDiv);
    } else if (checked === "Non-Academic") {
        document.getElementById('non-academic-rubric-evidence').append(evidenceSnapshotDiv);
    } else if (checked === "Alignment") {
        document.getElementById('alignment-rubric-evidence').append(evidenceSnapshotDiv);
    }

    document.getElementById("evidence_div_" + index).addEventListener('click', function() {
        editCard(index);
    });

    document.getElementById("evidence_div_" + index).addEventListener('dragstart', function() {
        drag(event, index);
    });
}

/**
 * When the 'Erase Evidence' button is clicked on, the UI of the card will be deleted
 * as well as the actual data that is stored in the cards dictionary. The card's index,
 * however, will not be reused.
 * @param button The 'Erase Evidence' button that has been clicked on.
 */
function erase_evidence(button){
    let id = button.id;
    let index = id.split('_')[3];
    delete cards[index];
    document.getElementById("flip-card-" + index).remove();
}
/**
 * A function used in the dragging of the evidence card. This function is what allows
 * for the middle div to have the card dropped on. This acts to qualify the middle
 * div as a final destination for the card during the drag cycle.
 * @param event The mouse/object dragging event
 */
function allowDrop(event) {
    event.preventDefault();
}

/**
 * A function used in the dragging of the evidence card. This function is invoked
 * at the start of the drag cycle.
 * @param event The mouse/object dragging event
 * @param index The id/key of the card that is being moved.
 */
function drag(event, index) {
    moving_card_index = index;
}

/**
 * A function used in the dragging of the evidence card. This function is invoked
 * at the end of the drag cycle.
 * @param event The mouse/object dragging event
 */
function drop(event) {
    event.preventDefault();
    editCard(moving_card_index);
}
# Evidence Capture

## About
Evidence Capture is a tool used to record evidence embedded in a PDF document 
that relates to some evaluation of a student or assignment. Once the evidence
is captured and annotated using a provided rubric, a wholistic overview of the
captured evidence is updated. This overview can be viewed at anytime. As of now,
rubrics are not interchangeable, however later versions will support the ability
to provide your own rubric.

## Getting Started
The application is hosted at https://irishintel.com/evidence
1. Begin by clicking the button titled "Choose File" to select a PDF file from 
your filesystem.
2. Once the file is selected, press the "Load PDF" button to render the contents
of the document in the application.
3. Use the "Previous" and "Next" buttons to navigate between pages of the document.
4. To make a selection and capture part of the document, press the "Capture" button.
Note that once this has been pressed, the mode will be set to capture, until the 
page reloads or the "Pointer" button is pressed.
5. Click and drag of a selection of the PDF to indicate the region that is to be 
captured.
6. After the selection is made, a card will appear in the middle column which is 
where you can add notes and indicate which category of the rubric it identifies 
with. You can also adjust the slider value to associate a value with the evidence.
7. By pressing "Save Evidence" the evidence will be saved and will be stored in its
respective category. However, pressing "Erase Evidence" will permanently delete the
evidence.
8. Once the evidence is in the grouping in the rightmost column, it will be stored for
later assessment. However, by clicking and dragging the snapshot of the evidence back
to the middle column, you can edit the details and category.

## Developing
1. Clone this repository anywhere on your local device:
`git clone https://gitlab.com/ecunnin2/pdf-genius.git`
2. Load pdf-genius/index.html in any browser
